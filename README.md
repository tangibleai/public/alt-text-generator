# Alt Image Generator

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/nutlope/alt-text-generator&env=REPLICATE_API_KEY&project-name=alt-tag-generator&repo-name=alt-tag-generator)

This Flask API will generate a description for any image using AI. If you're looking for the TypeScript version, [click here](https://github.com/vercel/examples/tree/main/solutions/alt-tag-generator).

![Alt Image Generator](ogimage.png)

## How it works

This project uses an ML modal from Salesforce called [BLIP](https://github.com/salesforce/BLIP) on [Replicate](https://replicate.com/) to generate relevant alt text for images. You can feed the Flask API endpoint an image as a query param and it will return a one sentence description of that image.

## Running Locally

1. Clone this repo
1. Copy the `example.env` to `.env` and replace the REPLICATE_API_TOKEN
1. Sign up at [Replicate](https://replicate.com/)
1. Copy your replicate.com API TOKEN (from your Account Profile) to the `.env` file.

Run the following in the commands to install `flask`, `python-dotenv` and `replicate`.

```bash
$ pip install --upgrade virtualenv
$ python -m virtualenv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ python -m flask --app api/index.html run
```

Use `curl` or a browser to visit a url with one `imageUrl` GET parameter pointing to an image you would like to have captioned:

```bash
IMAGEURL=https://gitlab.com/tangibleai/public/alt-text-generator/-/raw/main/ogimage.png
curl http://localhost:5000/generate?imageUrl=$IMAGEURL
```

OR

```bash
firefox http://localhost:5000/generate?$IMAGEURL
```

You can also just visit `http://localhost:5000` to see the Flask landing page for your local API.
