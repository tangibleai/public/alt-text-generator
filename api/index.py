import logging
import os

import dotenv
import replicate
from flask import Flask, request

log = logging.getLogger(__name__)
dotenv.load_dotenv()

app = Flask(__name__)
REPLICATE_API_TOKEN = os.environ.get("REPLICATE_API_TOKEN")
log.error(f"REPLICATE_API_TOKEN: {REPLICATE_API_TOKEN}")


@app.route("/")
def index():
    IMAGEURL = 'https://gitlab.com/tangibleai/public/alt-text-generator/-/raw/main/ogimage.png'
    return (
        '<p>Generate ALT text for your image at:</p>'
        f'<a href="http://127.0.0.1:5000/generate?imageUrl={IMAGEURL}">'
        'http://localhost:5000/generate?imageUrl=''https://{IMAGEURL}</a>'
    )


@app.route('/generate')
def generate():
    args = request.args
    imageUrl = args.to_dict().get('imageUrl')
    model = replicate.models.get("salesforce/blip")
    version = model.versions.get("2e1dddc8621f72155f24cf2e0adbde548458d3cab9f00c0139eea840d0ac4746")
    return version.predict(image=imageUrl)
